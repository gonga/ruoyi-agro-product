package com.gzq.service;

import com.gzq.domain.TestUser;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
* @author 占全
* @description 针对表【test_user】的数据库操作Service
* @createDate 2024-03-03 16:16:35
*/
public interface TestUserService extends IService<TestUser> {

    void outDate(String path, Integer count);

    void readData(MultipartFile file);

    void readDataPath(String path);

    List<TestUser> listByPage(Integer pageNum, Integer pageSize);
}
