package com.gzq.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gzq.domain.Pages;
import com.gzq.domain.TestUser;
import com.gzq.my.handler.ExcelFillCellMergeStrategy;
import com.gzq.service.TestUserService;
import com.gzq.mapper.TestUserMapper;
import com.gzq.my.listenner.EasyExcelListenner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

/**
* @author 占全
* @description 针对表【test_user】的数据库操作Service实现
* @createDate 2024-03-03 16:16:35
*/
@Service
public class TestUserServiceImpl extends ServiceImpl<TestUserMapper, TestUser>
    implements TestUserService{

    @Autowired
    TestUserMapper userMapper;
    @Autowired
    TestUserService userService;

    @Override
    public void outDate(String path, Integer count) {

        long l = System.currentTimeMillis();
        //创建一个excel文件
        //ExcelWriter excelWriter = EasyExcel.write(path, com.ruoyi.domain.TestUser.class).build();
        //通过计算得出需要sheet的个数
        int sheetNum = count % Pages.PAGE_SIZE == 0 ? count / Pages.PAGE_SIZE : count / Pages.PAGE_SIZE + 1;

        for (int i =1; i <= sheetNum; i++) {
            PageHelper.startPage(i,Pages.PAGE_SIZE);
            List<TestUser> list = userService.list();
            PageInfo<TestUser> testUserPageInfo = new PageInfo<>(list);
            List<TestUser> listByPage = testUserPageInfo.getList();

            //不合并单元格
            /*WriteSheet sheet = EasyExcel.writerSheet("sheet" + i).build();
            excelWriter.write(listByPage, sheet);
            excelWriter.finish();*/
        //合并单元格 合并哪几列
        int[] mergeColumnIndex = {0,1,3,5};
        // 需要从第几行开始合并
        int mergeRowIndex = 1;
        EasyExcel.write(path, TestUser.class)
                .autoCloseStream(Boolean.TRUE)
                .registerWriteHandler(new ExcelFillCellMergeStrategy(mergeRowIndex,mergeColumnIndex))
                .sheet("sheet"+i)
                .doWrite(listByPage);
        }
        System.out.println("导出成功");
        long l1 = System.currentTimeMillis();
        System.out.println(l1 - l);


    }

    @Override
    public void readData(MultipartFile file) {
        try {
            InputStream inputStream = file.getInputStream();
            ExcelReader excelReader = EasyExcel.read(inputStream, TestUser.class, new EasyExcelListenner()).doReadAll();
            excelReader.finish();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public void readDataPath(String path) {
        try {

            ExcelReader excelReader = EasyExcel.read(path, TestUser.class, new EasyExcelListenner()).doReadAll();
            excelReader.finish();

        }catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    public List<TestUser> listByPage(Integer pageNum, Integer pageSize) {
        return userMapper.listByPage(pageNum,pageSize);
    }
}




