package com.gzq.mapper;

import com.gzq.domain.TestUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
* @author 占全
* @description 针对表【test_user】的数据库操作Mapper
* @createDate 2024-03-03 16:16:35
* @Entity com.gzq.domain.TestUser
*/
public interface TestUserMapper extends BaseMapper<TestUser> {

    List<TestUser> listByPage(@Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);
}




