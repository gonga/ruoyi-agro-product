package com.gzq.domain;

import lombok.Data;

@Data
public class Pages {

    public static final int PAGE_SIZE = 50*1000;
}
