package com.gzq.domain;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.gzq.my.config.SexConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @TableName test_user
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestUser implements Serializable {
    @ExcelIgnore
    private Integer id;
    @ExcelProperty(value = "部门id")
    private Integer deptId;

    @ExcelProperty(value = "登录名称")
    private String loginName;

    @ExcelProperty(value ="真实名称")
    private String username;

    @ExcelProperty(value ="邮箱")
    private String email;

    @ExcelProperty(value = "联系方式")
    private String phone;

    @ExcelProperty(value = "性别",converter = SexConverter.class)
    private Integer sex;
    @ExcelProperty(value = "创建时间")
    @com.alibaba.excel.annotation.format.DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    private Date createDate;



    @ExcelProperty(value = "状态")
    private Integer status;

    private static final long serialVersionUID = 1L;
}
