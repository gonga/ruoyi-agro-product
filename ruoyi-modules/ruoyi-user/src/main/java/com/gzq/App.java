package com.gzq;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
@MapperScan("com.gzq.mapper")
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class,args);
        System.out.println("服务启动成功");
    }
}
