package com.gzq.my.config;

import lombok.Data;

import java.io.Serializable;


@Data
public class R<T> implements Serializable {
    private Integer code;
    private String msg;
    private T data;

    public static <T> R successed(T data){
        R resultModel = new R();
        resultModel.setCode(200);
        resultModel.setMsg("操作成功");
        resultModel.setData(data);
        return resultModel;
    }
    public static <T> R successed(){
        R resultModel = new R();
        resultModel.setCode(200);
        resultModel.setMsg("操作成功");
        return resultModel;
    }
    public static R failed(Integer code, String msg){
        R resultModel = new R();
        resultModel.setCode(code);
        resultModel.setMsg(msg);
        resultModel.setData(null);
        return resultModel;
    }
    public static R failed(String msg){
        R resultModel = new R();
        resultModel.setCode(500);
        resultModel.setMsg(msg);
        resultModel.setData(null);
        return resultModel;
    }
}


