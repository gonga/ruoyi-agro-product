package com.ruoyi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName rawmaterial_info
 */
@TableName(value ="rawmaterial_info")
@Data
public class RawmaterialInfo implements Serializable {
    /**
     * 主键id  
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 编号
     */
    private Long rawmaterialNum;

    /**
     * 加工厂id 
     */
    private Integer factoryId;

    /**
     * 农产品id
     */
    private Integer cropId;

    /**
     * 类别id  字典表dictionary 
     */
    private Integer materialId;

    /**
     * 规格id  字典表dictionary 
     */
    private Integer unitId;

    /**
     * 执行标准id  字典表dictionary
     */
    private Integer standardId;

    /**
     * 库存量
     */
    private String inventory;

    /**
     * 采购价格
     */
    private BigDecimal purchasePrice;

    /**
     * 入库日期
     */
    private Date wareDate;

    /**
     * 出库日期
     */
    private Date delDate;

    /**
     * 相关文档（质量检测报告
     */
    private String document;

    /**
     * 原来料类别
     */
    @TableField(exist = false)
    private String matename;

    /**
     * 规格
     */
    @TableField(exist = false)
    private String unitname;

    /**
     * 执行类别
     */
    @TableField(exist = false)
    private String standname;

    /**
     * 工厂
     */
    private String factoryname;

    /**
 * 状态 删除时
     */
    private String status;

    /**
     * 农作物名称
     */
    private String cropname;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}