package com.ruoyi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.domain.Dictionary;
import com.ruoyi.domain.vo.DictionaryVo;
import com.ruoyi.service.DictionaryService;
import com.ruoyi.mapper.DictionaryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 53545
* @description 针对表【dictionary(农产品溯源字典表)】的数据库操作Service实现
* @createDate 2024-02-28 15:31:29
*/
@Service
public class DictionaryServiceImpl extends ServiceImpl<DictionaryMapper, Dictionary>
    implements DictionaryService{

    @Autowired
    DictionaryMapper dictionaryMapper;

    @Override
    public List<Dictionary> getStandardListByType(String type) {
        return dictionaryMapper.getStandardListByType(type);
    }

    @Override
    public List<DictionaryVo> getopertor(String type) {
        return dictionaryMapper.getopertor(type);
    }

    @Override
    public List<DictionaryVo> getdicById(Integer id) {
        return dictionaryMapper.getdicById(id);
    }

    @Override
    public String getNameById(String opertor) {
        return dictionaryMapper.getNameById(opertor);
    }


}




