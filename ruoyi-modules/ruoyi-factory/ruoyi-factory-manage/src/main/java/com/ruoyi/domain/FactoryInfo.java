package com.ruoyi.domain;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @TableName factory_info
 */
@TableName(value ="factory_info")
@Data
public class FactoryInfo implements Serializable {
    /**
     * 主键id 
     */
    @TableId(type = IdType.AUTO)
    @ExcelProperty(index = 0,value = {"工厂Id"})
    private Integer factoryId;

    /**
     * 编号
     */
    @ExcelProperty(index = 1,value = {"工厂编号"})
    private Long factoryNum;

    /**
     * 工厂类型id 字典表dictionary     type=company_type
     */
    @ExcelProperty
    private Integer companyId;


    /**
     * 名称 
     */
    @ExcelProperty(index = 2,value = {"工厂名称"})
    private String companyName;

    /**
     * 法人
     */
    @ExcelProperty(index = 3,value = {"法人"})
    private String legalPerson;

    /**
     * 联系方式
     */
    @ExcelProperty(index = 4,value = {"联系方式"})
    private String phone;

    /**
     * 地址 
     */
    @ExcelProperty(index = 5,value = {"工厂地址"})
    private String address;

    /**
     * 创建时间
     */
    @ExcelProperty(index = 6,value = {"创建时间"})
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /**
     * 状态  
     */
    @ExcelProperty(index = 7,value = {"工厂是否在使用"})
    private String status;

    @TableField(exist = false)
    @ExcelProperty(index = 8,value = {"工厂类型"})
    private String name;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}