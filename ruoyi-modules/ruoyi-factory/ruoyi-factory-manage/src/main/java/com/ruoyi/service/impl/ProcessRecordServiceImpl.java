package com.ruoyi.service.impl;

import java.util.List;

import com.ruoyi.domain.ProcessRecord;
import com.ruoyi.mapper.ProcessRecordMapper;
import com.ruoyi.service.IProcessRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 加工记录跟踪Service业务层处理
 *
 * @author ruoyi
 * @date 2024-02-28
 */
@Service
public class ProcessRecordServiceImpl implements IProcessRecordService
{
    @Autowired
    private ProcessRecordMapper processRecordMapper;

    /**
     * 查询加工记录跟踪
     *
     * @param processId 加工记录跟踪主键
     * @return 加工记录跟踪
     */
    @Override
    public ProcessRecord selectProcessRecordByProcessId(Long processId)
    {
        return processRecordMapper.selectProcessRecordByProcessId(processId);
    }

    /**
     * 查询加工记录跟踪列表
     *
     * @param processRecord 加工记录跟踪
     * @return 加工记录跟踪
     */
    @Override
    public List<ProcessRecord> selectProcessRecordList(ProcessRecord processRecord)
    {
        return processRecordMapper.selectProcessRecordList(processRecord);
    }

    /**
     * 新增加工记录跟踪
     *
     * @param processRecord 加工记录跟踪
     * @return 结果
     */
    @Override
    public int insertProcessRecord(ProcessRecord processRecord)
    {
        return processRecordMapper.insertProcessRecord(processRecord);
    }

    /**
     * 修改加工记录跟踪
     *
     * @param processRecord 加工记录跟踪
     * @return 结果
     */
    @Override
    public int updateProcessRecord(ProcessRecord processRecord)
    {
        return processRecordMapper.updateProcessRecord(processRecord);
    }

    /**
     * 批量删除加工记录跟踪
     *
     * @param processIds 需要删除的加工记录跟踪主键
     * @return 结果
     */
    @Override
    public int deleteProcessRecordByProcessIds(Long[] processIds)
    {
        return processRecordMapper.updateProcessRecordByProcessIds(processIds);
    }

    /**
     * 删除加工记录跟踪信息
     *
     * @param processId 加工记录跟踪主键
     * @return 结果
     */
    @Override
    public int deleteProcessRecordByProcessId(Long processId)
    {
        return processRecordMapper.deleteProcessRecordByProcessId(processId);
    }

    @Override
    public void insertProductionInfoBatch(List<ProcessRecord> list) {
        processRecordMapper.insertProductionInfoBatch(list);
    }
}
