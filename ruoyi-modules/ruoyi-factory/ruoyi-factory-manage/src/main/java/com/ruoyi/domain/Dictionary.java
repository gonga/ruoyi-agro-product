package com.ruoyi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 农产品溯源字典表
 * @TableName dictionary
 */
@TableName(value ="dictionary")
@Data
public class Dictionary implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     *
     */

    private String name;

    /**
     *
     */

    private Long dictValue;

    /**
     *
     */
    private String type;

    /**
     *
     */

    private String remark;

    private List<Dictionary>  children;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
