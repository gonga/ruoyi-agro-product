package com.ruoyi.service;

import com.ruoyi.domain.ProcessRecord;

import java.util.List;

/**
 * 加工记录跟踪Service接口
 *
 * @author ruoyi
 * @date 2024-02-28
 */
public interface IProcessRecordService
{
    /**
     * 查询加工记录跟踪
     *
     * @param processId 加工记录跟踪主键
     * @return 加工记录跟踪
     */
    public ProcessRecord selectProcessRecordByProcessId(Long processId);

    /**
     * 查询加工记录跟踪列表
     *
     * @param processRecord 加工记录跟踪
     * @return 加工记录跟踪集合
     */
    public List<ProcessRecord> selectProcessRecordList(ProcessRecord processRecord);

    /**
     * 新增加工记录跟踪
     *
     * @param processRecord 加工记录跟踪
     * @return 结果
     */
    public int insertProcessRecord(ProcessRecord processRecord);

    /**
     * 修改加工记录跟踪
     *
     * @param processRecord 加工记录跟踪
     * @return 结果
     */
    public int updateProcessRecord(ProcessRecord processRecord);

    /**
     * 批量删除加工记录跟踪
     *
     * @param processIds 需要删除的加工记录跟踪主键集合
     * @return 结果
     */
    public int deleteProcessRecordByProcessIds(Long[] processIds);

    /**
     * 删除加工记录跟踪信息
     *
     * @param processId 加工记录跟踪主键
     * @return 结果
     */
    public int deleteProcessRecordByProcessId(Long processId);

    void insertProductionInfoBatch(List<ProcessRecord> list);
}
