package com.ruoyi.mapper;

import com.ruoyi.domain.Dictionary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.domain.vo.DictionaryVo;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
* @author 53545
* @description 针对表【dictionary(农产品溯源字典表)】的数据库操作Mapper
* @createDate 2024-02-28 15:31:29
* @Entity com.ruoyi.domain.Dictionary
*/
public interface DictionaryMapper extends BaseMapper<Dictionary> {

    List<Dictionary> getStandardListByType(@Param("type") String type);

    List<DictionaryVo> getopertor(@Param("type") String type);


    List<DictionaryVo> getdicById(Integer id);

    String getNameById(String opertor);
}




