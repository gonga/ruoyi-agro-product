package com.ruoyi.service;

import com.ruoyi.domain.ProductionInfo;

import java.util.List;

/**
 * 加工成品信息Service接口
 *
 * @author ruoyi
 * @date 2024-02-28
 */
public interface IProductionInfoService
{
    /**
     * 查询加工成品信息
     *
     * @param productionId 加工成品信息主键
     * @return 加工成品信息
     */
    public ProductionInfo selectProductionInfoByProductionId(Long productionId);

    /**
     * 查询加工成品信息列表
     *
     * @param productionInfo 加工成品信息
     * @return 加工成品信息集合
     */
    public List<ProductionInfo> selectProductionInfoList(ProductionInfo productionInfo);

    /**
     * 新增加工成品信息
     *
     * @param productionInfo 加工成品信息
     * @return 结果
     */
    public int insertProductionInfo(ProductionInfo productionInfo);

    /**
     * 修改加工成品信息
     *
     * @param productionInfo 加工成品信息
     * @return 结果
     */
    public int updateProductionInfo(ProductionInfo productionInfo);

    /**
     * 批量删除加工成品信息
     *
     * @param productionIds 需要删除的加工成品信息主键集合
     * @return 结果
     */
    public int deleteProductionInfoByProductionIds(Long[] productionIds);

    /**
     * 删除加工成品信息信息
     *
     * @param productionId 加工成品信息主键
     * @return 结果
     */
    public int deleteProductionInfoByProductionId(Long productionId);

    void insertProductionInfoBatch(List<ProductionInfo> list);
}
