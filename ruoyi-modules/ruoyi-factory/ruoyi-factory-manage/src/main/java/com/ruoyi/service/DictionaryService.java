package com.ruoyi.service;

import com.ruoyi.domain.Dictionary;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.domain.vo.DictionaryVo;

import java.util.List;

/**
* @author 53545
* @description 针对表【dictionary(农产品溯源字典表)】的数据库操作Service
* @createDate 2024-02-28 15:31:29
*/
public interface DictionaryService extends IService<Dictionary> {

    List<Dictionary> getStandardListByType(String type);

    List<DictionaryVo> getopertor(String type);


    List<DictionaryVo> getdicById(Integer id);

    String getNameById(String opertor);
}
