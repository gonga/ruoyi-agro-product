package com.ruoyi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.domain.RawmaterialInfo;
import com.ruoyi.mapper.RawmaterialInfoMapper;
import com.ruoyi.service.RawmaterialInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 53545
* @description 针对表【rawmaterial_info】的数据库操作Service实现
* @createDate 2024-02-28 08:36:28
*/
@Service
public class RawmaterialInfoServiceImpl extends ServiceImpl<RawmaterialInfoMapper, RawmaterialInfo>
    implements RawmaterialInfoService {

    @Autowired
    RawmaterialInfoMapper rawmaterialInfoMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public RawmaterialInfo selectRawmaterialInfoById(Long id)
    {
        return rawmaterialInfoMapper.selectRawmaterialInfoById(id);
    }


    /**
     * 查询【请填写功能名称】列表
     *
     * @param rawmaterialInfo 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<RawmaterialInfo> selectRawmaterialInfoList(RawmaterialInfo rawmaterialInfo)
    {
        return rawmaterialInfoMapper.selectRawmaterialInfoList(rawmaterialInfo);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param rawmaterialInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertRawmaterialInfo(RawmaterialInfo rawmaterialInfo)
    {
        return rawmaterialInfoMapper.insertRawmaterialInfo(rawmaterialInfo);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param rawmaterialInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateRawmaterialInfo(RawmaterialInfo rawmaterialInfo)
    {
        return rawmaterialInfoMapper.updateRawmaterialInfo(rawmaterialInfo);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteRawmaterialInfoByIds(Long[] ids)
    {
        return rawmaterialInfoMapper.deleteRawmaterialInfoByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteRawmaterialInfoById(Long id)
    {
        return rawmaterialInfoMapper.deleteRawmaterialInfoById(id);
    }

    @Override
    public List<RawmaterialInfo> getNumList() {
        return rawmaterialInfoMapper.getNumList();
    }
}




