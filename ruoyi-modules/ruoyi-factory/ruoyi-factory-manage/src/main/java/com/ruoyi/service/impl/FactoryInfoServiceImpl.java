package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.service.FactoryInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.FactoryInfoMapper;
import com.ruoyi.domain.FactoryInfo;
import com.ruoyi.service.FactoryInfoService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-02-28
 */
@Service
public class FactoryInfoServiceImpl implements FactoryInfoService
{
    @Autowired
    private FactoryInfoMapper factoryInfoMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param factoryId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public FactoryInfo selectFactoryInfoByFactoryId(Long factoryId)
    {
        return factoryInfoMapper.selectFactoryInfoByFactoryId(factoryId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param factoryInfo 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<FactoryInfo> selectFactoryInfoList(FactoryInfo factoryInfo)
    {
        return factoryInfoMapper.selectFactoryInfoList(factoryInfo);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param factoryInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertFactoryInfo(FactoryInfo factoryInfo)
    {
        factoryInfo.setCreateTime(DateUtils.getNowDate());
        return factoryInfoMapper.insertFactoryInfo(factoryInfo);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param factoryInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateFactoryInfo(FactoryInfo factoryInfo)
    {
        return factoryInfoMapper.updateFactoryInfo(factoryInfo);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param factoryIds 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteFactoryInfoByFactoryIds(Long[] factoryIds)
    {
        return factoryInfoMapper.deleteFactoryInfoByFactoryIds(factoryIds);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param factoryId 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteFactoryInfoByFactoryId(Long factoryId)
    {
        return factoryInfoMapper.deleteFactoryInfoByFactoryId(factoryId);
    }

    @Override
    public List<FactoryInfo> selectFactoryInfoListAll() {
        return factoryInfoMapper.selectFactoryInfoListAll();
    }

    @Override
    public List<FactoryInfo> bingtu(String name, String address) {
        return factoryInfoMapper.bingtu(name,address);
    }
}
