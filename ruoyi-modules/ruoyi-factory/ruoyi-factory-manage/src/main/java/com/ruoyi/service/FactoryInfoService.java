package com.ruoyi.service;

import java.util.List;
import com.ruoyi.domain.FactoryInfo;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2024-02-28
 */
public interface FactoryInfoService
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param factoryId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public FactoryInfo selectFactoryInfoByFactoryId(Long factoryId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param factoryInfo 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<FactoryInfo> selectFactoryInfoList(FactoryInfo factoryInfo);

    /**
     * 新增【请填写功能名称】
     * 
     * @param factoryInfo 【请填写功能名称】
     * @return 结果
     */
    public int insertFactoryInfo(FactoryInfo factoryInfo);

    /**
     * 修改【请填写功能名称】
     * 
     * @param factoryInfo 【请填写功能名称】
     * @return 结果
     */
    public int updateFactoryInfo(FactoryInfo factoryInfo);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param factoryIds 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteFactoryInfoByFactoryIds(Long[] factoryIds);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param factoryId 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteFactoryInfoByFactoryId(Long factoryId);

    List<FactoryInfo> selectFactoryInfoListAll();

    List<FactoryInfo> bingtu(String name, String address);
}
