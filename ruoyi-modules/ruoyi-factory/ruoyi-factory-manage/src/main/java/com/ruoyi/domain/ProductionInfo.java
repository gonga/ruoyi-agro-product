package com.ruoyi.domain;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.utils.DatetimeConverter;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 加工成品信息对象 production_info
 *
 * @author ruoyi
 * @date 2024-02-28
 */
@Data
@ExcelIgnoreUnannotated
public class ProductionInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 加工成品ID */
    @ExcelIgnore
    private Long productionId;

    /** 成品编号 */

    @ExcelProperty(value = "成品编号",index = 0)
    private String productionNum;

    /** 成品名称 */

    @ExcelProperty(value = "成品名称",index = 1)
    private String productionName;

    /** 数量 */

    @ExcelProperty(value = "数量",index = 2)
    private Long productionQuantity;

    /** 原材料ID */
    @ExcelProperty(value = "原料id")
    private Long rawmaterialId;

    @ExcelProperty(value = "原料编号",index = 3)
    private String rawmaterialNum;

    /** 执行标准ID（字典表 standard_type） */

    @ExcelProperty(value = "执行标准id")
    private Long standardId;

    public String getRawmaterialNum() {
        return rawmaterialNum;
    }

    public void setRawmaterialNum(String rawmaterialNum) {
        this.rawmaterialNum = rawmaterialNum;
    }

    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    @ExcelProperty(value = "执行标准",index = 4)
    private String standardName;


    /** 生产日期 */
    //@JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(value = "生产日期",index = 5,converter = DatetimeConverter.class)
    @DateTimeFormat("yyyy-MM-dd")
    private Date productionDate;

    @Override
    public String toString() {
        return "ProductionInfo{" +
                "productionId=" + productionId +
                ", productionNum='" + productionNum + '\'' +
                ", productionName='" + productionName + '\'' +
                ", productionQuantity=" + productionQuantity +
                ", rawmaterialId=" + rawmaterialId +
                ", rawmaterialNum='" + rawmaterialNum + '\'' +
                ", standardId=" + standardId +
                ", standardName='" + standardName + '\'' +
                ", productionDate=" + productionDate +
                ", shelf='" + shelf + '\'' +
                ", status='" + status + '\'' +
                ", document='" + document + '\'' +
                ", pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                ", begin=" + begin +
                ", end=" + end +
                '}';
    }

    /** 保质期（月） */

    @ExcelProperty(value = "保质期",index = 6)
    private String shelf;

    /** 状态 */

    @ExcelIgnore
    private String status;

    /** 相关文档（质量检测报告） */

    @ExcelProperty(value = "合格证明",index = 7)
    private String document;




    public void setProductionId(Long productionId)
    {
        this.productionId = productionId;
    }

    public Long getProductionId()
    {
        return productionId;
    }
    public void setProductionNum(String productionNum)
    {
        this.productionNum = productionNum;
    }

    public String getProductionNum()
    {
        return productionNum;
    }
    public void setProductionName(String productionName)
    {
        this.productionName = productionName;
    }

    public String getProductionName()
    {
        return productionName;
    }
    public void setProductionQuantity(Long productionQuantity)
    {
        this.productionQuantity = productionQuantity;
    }

    public Long getProductionQuantity()
    {
        return productionQuantity;
    }
    public void setRawmaterialId(Long rawmaterialId)
    {
        this.rawmaterialId = rawmaterialId;
    }

    public Long getRawmaterialId()
    {
        return rawmaterialId;
    }
    public void setStandardId(Long standardId)
    {
        this.standardId = standardId;
    }

    public Long getStandardId()
    {
        return standardId;
    }
    public void setProductionDate(Date productionDate)
    {
        this.productionDate = productionDate;
    }

    public Date getProductionDate()
    {
        return productionDate;
    }
    public void setShelf(String shelf)
    {
        this.shelf = shelf;
    }

    public String getShelf()
    {
        return shelf;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDocument(String document)
    {
        this.document = document;
    }

    public String getDocument()
    {
        return document;
    }


    @ExcelIgnore
    private  Integer pageNum;
    @ExcelIgnore
    private Integer pageSize;

    @ExcelIgnore
    private Date begin;
    @ExcelIgnore
    private Date end;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
