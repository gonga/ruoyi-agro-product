package com.ruoyi.domain;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.utils.ProcessDatetimeConverter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 加工记录跟踪对象 process_record
 *
 * @author ruoyi
 * @date 2024-02-28
 */
@ExcelIgnoreUnannotated
public class ProcessRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long processId;

    /** 原料id */
    @Excel(name = "原料id")
    private Long rawmaterialId;

    public String getRawmaterialNum() {
        return rawmaterialNum;
    }

    public void setRawmaterialNum(String rawmaterialNum) {
        this.rawmaterialNum = rawmaterialNum;
    }
@ExcelProperty(value = "原料编号")
    private String rawmaterialNum;

    /** 加工进度id */
    @Excel(name = "加工进度id")
    private Long scheduleId;

    @ExcelProperty("加工进度")
    private String scheduleName;

    /** 具体操作名称 */
    @Excel(name = "具体操作名称")
    @ExcelProperty("具体操作")
    private String operationName;

    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ExcelProperty(value = "操作时间",converter = ProcessDatetimeConverter.class)
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    private Date operationTime;

    /** 操作员 */
    @Excel(name = "操作员")
    @ExcelProperty("操作员")
    private String opertor;

    /** 操作人员联系方式 */
    @Excel(name = "操作人员联系方式")
    @ExcelProperty("联系方式")
    private String opertorPhone;

    /** 加工设备id */
    @Excel(name = "加工设备id")
    private Long equipmentId;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    private Integer status;


    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    @ExcelProperty("加工设备")
    private String equipmentName;

    /** 压力 */
    @Excel(name = "压力")
    @ExcelProperty("压力")
    private String stress;

    /** 温度 */
    @Excel(name = "温度")

    @ExcelProperty("温度")
    private String temperature;

    /** 湿度 */
    @Excel(name = "湿度")
    @ExcelProperty("湿度")
    private String humidness;

    /** 图片 */
    @Excel(name = "图片")
    @ExcelProperty("图片链接")
    private String pic;

    public void setProcessId(Long processId)
    {
        this.processId = processId;
    }

    public Long getProcessId()
    {
        return processId;
    }
    public void setRawmaterialId(Long rawmaterialId)
    {
        this.rawmaterialId = rawmaterialId;
    }

    @Override
    public String toString() {
        return "ProcessRecord{" +
                "processId=" + processId +
                ", rawmaterialId=" + rawmaterialId +
                ", rawmaterialNum='" + rawmaterialNum + '\'' +
                ", scheduleId=" + scheduleId +
                ", scheduleName='" + scheduleName + '\'' +
                ", operationName='" + operationName + '\'' +
                ", operationTime=" + operationTime +
                ", opertor='" + opertor + '\'' +
                ", opertorPhone='" + opertorPhone + '\'' +
                ", equipmentId=" + equipmentId +
                ", status=" + status +
                ", equipmentName='" + equipmentName + '\'' +
                ", stress='" + stress + '\'' +
                ", temperature='" + temperature + '\'' +
                ", humidness='" + humidness + '\'' +
                ", pic='" + pic + '\'' +
                '}';
    }

    public Long getRawmaterialId()
    {
        return rawmaterialId;
    }
    public void setScheduleId(Long scheduleId)
    {
        this.scheduleId = scheduleId;
    }

    public Long getScheduleId()
    {
        return scheduleId;
    }
    public void setOperationName(String operationName)
    {
        this.operationName = operationName;
    }

    public String getOperationName()
    {
        return operationName;
    }
    public void setOperationTime(Date operationTime)
    {
        this.operationTime = operationTime;
    }

    public Date getOperationTime()
    {
        return operationTime;
    }
    public void setOpertor(String opertor)
    {
        this.opertor = opertor;
    }

    public String getOpertor()
    {
        return opertor;
    }
    public void setOpertorPhone(String opertorPhone)
    {
        this.opertorPhone = opertorPhone;
    }

    public String getOpertorPhone()
    {
        return opertorPhone;
    }
    public void setEquipmentId(Long equipmentId)
    {
        this.equipmentId = equipmentId;
    }

    public Long getEquipmentId()
    {
        return equipmentId;
    }
    public void setStress(String stress)
    {
        this.stress = stress;
    }

    public String getStress()
    {
        return stress;
    }
    public void setTemperature(String temperature)
    {
        this.temperature = temperature;
    }

    public String getTemperature()
    {
        return temperature;
    }
    public void setHumidness(String humidness)
    {
        this.humidness = humidness;
    }

    public String getHumidness()
    {
        return humidness;
    }
    public void setPic(String pic)
    {
        this.pic = pic;
    }

    public String getPic()
    {
        return pic;
    }

}
