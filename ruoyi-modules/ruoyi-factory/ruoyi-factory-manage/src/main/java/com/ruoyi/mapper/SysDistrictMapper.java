package com.ruoyi.mapper;

import com.ruoyi.domain.SysDistrict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.service.SysDistrictService;

import java.util.List;

/**
* @author 53545
* @description 针对表【sys_district(地区表)】的数据库操作Mapper
* @createDate 2024-02-29 14:00:02
* @Entity com.ruoyi.domain.SysDistrict
*/
public interface SysDistrictMapper extends BaseMapper<SysDistrict> {

    List<SysDistrict> listAll();
}




