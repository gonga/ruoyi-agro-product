package com.ruoyi.Listener;


import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.ruoyi.domain.FactoryInfo;
import io.swagger.models.auth.In;

import java.util.ArrayList;
import java.util.List;

public class FactoryListener extends AnalysisEventListener {

    List<String> list = new ArrayList<>();
    @Override
    public void invoke(Object data, AnalysisContext analysisContext) {
        String misCode = ((FactoryInfo) data).getCompanyName();
        list.add(misCode);
        System.err.println("导入一条输入成功！"+((FactoryInfo) data));
    }



    @Override
    public void onException(Exception exception, AnalysisContext context) throws Exception {
        if(exception instanceof ExcelDataConvertException){
            /*从0开始计算*/
            Integer columnIndex = ((ExcelDataConvertException) exception).getColumnIndex() + 1;
            Integer rowIndex = ((ExcelDataConvertException) exception).getRowIndex() + 1;
            String message = "第" + rowIndex + "行,第" + columnIndex + "列" + "数据格式有误,请核实";
            throw new RuntimeException(message);
        }else if(exception instanceof  ExcelDataConvertException){
            throw exception;
        }else{
            super.onException(exception, context);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        //解析玩,全部回调逻辑实现
        System.err.println("一共导入了" + list.size()+ "条数据");
        list.clear();
    }
}
