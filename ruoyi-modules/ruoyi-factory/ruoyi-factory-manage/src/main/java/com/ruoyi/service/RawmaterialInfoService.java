package com.ruoyi.service;

import java.util.List;

import com.ruoyi.domain.RawmaterialInfo;
/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2024-02-28
 */
public interface RawmaterialInfoService
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public RawmaterialInfo selectRawmaterialInfoById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param rawmaterialInfo 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<RawmaterialInfo> selectRawmaterialInfoList(RawmaterialInfo rawmaterialInfo);

    /**
     * 新增【请填写功能名称】
     * 
     * @param rawmaterialInfo 【请填写功能名称】
     * @return 结果
     */
    public int insertRawmaterialInfo(RawmaterialInfo rawmaterialInfo);

    /**
     * 修改【请填写功能名称】
     * 
     * @param rawmaterialInfo 【请填写功能名称】
     * @return 结果
     */
    public int updateRawmaterialInfo(RawmaterialInfo rawmaterialInfo);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteRawmaterialInfoByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteRawmaterialInfoById(Long id);

    List<RawmaterialInfo> getNumList();
}
