package com.ruoyi.service;

import com.ruoyi.domain.SysDistrict;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 53545
* @description 针对表【sys_district(地区表)】的数据库操作Service
* @createDate 2024-02-29 14:00:02
*/
public interface SysDistrictService extends IService<SysDistrict> {



    List<SysDistrict> listAll();
}
