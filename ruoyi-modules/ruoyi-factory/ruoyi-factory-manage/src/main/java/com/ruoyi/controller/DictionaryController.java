package com.ruoyi.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import com.ruoyi.domain.Dictionary;
import com.ruoyi.domain.vo.DictionaryVo;
import com.ruoyi.service.DictionaryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 农产品溯源字典Controller
 *
 * @author ruoyi
 * @date 2024-02-28
 */
@RestController
@RequestMapping("/dictionary")
public class DictionaryController extends BaseController
{
    @Autowired
    private DictionaryService dictionaryService;

    /**
     * 根据type 获取相应字典表的信息
     * @param type
     * @return
     */
    @GetMapping("getStandardList")
    public List<Dictionary> getStandardList( String type){
       List<Dictionary> list= dictionaryService.getStandardListByType(type);
       return list;
    }




    /**
     * 获取有关操作人员
     */
    @GetMapping("getopertor")
    public List<DictionaryVo> getopertorName(){
        List<DictionaryVo> getopertor = dictionaryService.getopertor("opertor");
        for (DictionaryVo dictionary : getopertor) {
          List<DictionaryVo> list=  dictionaryService.getdicById(dictionary.getId());
          dictionary.setChildren(list);
        }

        return getopertor;
    }


}
