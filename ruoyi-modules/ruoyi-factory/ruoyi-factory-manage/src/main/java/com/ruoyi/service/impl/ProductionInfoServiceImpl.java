package com.ruoyi.service.impl;

import java.util.List;

import com.ruoyi.domain.ProductionInfo;
import com.ruoyi.mapper.ProductionInfoMapper;
import com.ruoyi.service.IProductionInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 加工成品信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-02-28
 */
@Service
public class ProductionInfoServiceImpl implements IProductionInfoService
{
    @Autowired
    private ProductionInfoMapper productionInfoMapper;

    /**
     * 查询加工成品信息
     *
     * @param productionId 加工成品信息主键
     * @return 加工成品信息
     */
    @Override
    public ProductionInfo selectProductionInfoByProductionId(Long productionId)
    {
        return productionInfoMapper.selectProductionInfoByProductionId(productionId);
    }

    /**
     * 查询加工成品信息列表
     *
     * @param productionInfo 加工成品信息
     * @return 加工成品信息
     */
    @Override
    public List<ProductionInfo> selectProductionInfoList(ProductionInfo productionInfo)
    {
        return productionInfoMapper.selectProductionInfoList(productionInfo);
    }

    /**
     * 新增加工成品信息
     *
     * @param productionInfo 加工成品信息
     * @return 结果
     */
    @Override
    public int insertProductionInfo(ProductionInfo productionInfo)
    {
        return productionInfoMapper.insertProductionInfo(productionInfo);
    }

    /**
     * 修改加工成品信息
     *
     * @param productionInfo 加工成品信息
     * @return 结果
     */
    @Override
    public int updateProductionInfo(ProductionInfo productionInfo)
    {
        return productionInfoMapper.updateProductionInfo(productionInfo);
    }

    /**
     * 批量删除加工成品信息
     *
     * @param productionIds 需要删除的加工成品信息主键
     * @return 结果
     */
    @Override
    public int deleteProductionInfoByProductionIds(Long[] productionIds)
    {
        return productionInfoMapper.updateProductionInfoByProductionIds(productionIds);
    }

    /**
     * 删除加工成品信息信息
     *
     * @param productionId 加工成品信息主键
     * @return 结果
     */
    @Override
    public int deleteProductionInfoByProductionId(Long productionId)
    {
        return productionInfoMapper.deleteProductionInfoByProductionId(productionId);
    }

    @Override
    public void insertProductionInfoBatch(List<ProductionInfo> list) {
        productionInfoMapper.insertProductionInfoBatch(list);
    }
}
