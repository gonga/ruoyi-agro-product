package com.ruoyi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.domain.SysDistrict;
import com.ruoyi.service.SysDistrictService;
import com.ruoyi.mapper.SysDistrictMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
* @author 53545
* @description 针对表【sys_district(地区表)】的数据库操作Service实现
* @createDate 2024-02-29 14:00:02
*/
@Service
public class SysDistrictServiceImpl extends ServiceImpl<SysDistrictMapper, SysDistrict>
    implements SysDistrictService{

    @Autowired
    SysDistrictMapper sysDistrictMapper;

    @Override
    public List<SysDistrict> listAll() {
        return sysDistrictMapper.listAll();
    }
}




