package com.ruoyi.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 农产品溯源字典表
 * @TableName dictionary
 */
@TableName(value ="dictionary")
@Data
public class DictionaryVo implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    @JsonProperty(value = "value")
    private Integer id;

    /**
     *
     */
   @JsonProperty(value = "label")
    private String name;

    /**
     *
     */

    private Long dictValue;

    /**
     *
     */
    private String type;

    /**
     *
     */

    private String remark;

    private List<DictionaryVo>  children;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
