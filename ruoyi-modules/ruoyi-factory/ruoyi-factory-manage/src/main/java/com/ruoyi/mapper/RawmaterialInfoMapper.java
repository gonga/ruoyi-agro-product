package com.ruoyi.mapper;

import com.ruoyi.domain.RawmaterialInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author 53545
* @description 针对表【rawmaterial_info】的数据库操作Mapper
* @createDate 2024-02-28 08:36:28
* @Entity com.ruoyi.domain.RawmaterialInfo
*/
public interface RawmaterialInfoMapper extends BaseMapper<RawmaterialInfo> {

    List<RawmaterialInfo> selectRawmaterialInfoList(RawmaterialInfo rawmaterialInfo);

    int insertRawmaterialInfo(RawmaterialInfo rawmaterialInfo);

    int updateRawmaterialInfo(RawmaterialInfo rawmaterialInfo);

    int deleteRawmaterialInfoByIds(Long[] ids);

    int deleteRawmaterialInfoById(Long id);

    RawmaterialInfo selectRawmaterialInfoById(Long id);

    List<RawmaterialInfo> getNumList();
}




