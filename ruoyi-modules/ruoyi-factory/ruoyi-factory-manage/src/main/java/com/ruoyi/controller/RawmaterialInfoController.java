package com.ruoyi.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.IdUtil;
import com.ruoyi.domain.Dictionary;
import com.ruoyi.domain.RawmaterialInfo;
import com.ruoyi.service.DictionaryService;
import com.ruoyi.service.RawmaterialInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2024-02-28
 */
@RestController
@RequestMapping("/rawmaterial")
public class RawmaterialInfoController extends BaseController
{
    @Autowired
    private RawmaterialInfoService rawmaterialInfoService;

    @Autowired
    DictionaryService dictionaryService;

    /**
     * 查询【请填写功能名称】列表
     */
    @GetMapping("/list")
    public TableDataInfo list(RawmaterialInfo rawmaterialInfo)
    {
        startPage();
        List<RawmaterialInfo> list = rawmaterialInfoService.selectRawmaterialInfoList(rawmaterialInfo);
        return getDataTable(list);
    }
//
//    /**
//     * 导出【请填写功能名称】列表
//     */
//    @RequiresPermissions("system:info:export")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, RawmaterialInfo rawmaterialInfo)
//    {
//        List<RawmaterialInfo> list = rawmaterialInfoService.selectRawmaterialInfoList(rawmaterialInfo);
//        ExcelUtil<RawmaterialInfo> util = new ExcelUtil<RawmaterialInfo>(RawmaterialInfo.class);
//        util.exportExcel(response, list, "【请填写功能名称】数据");
//    }

    /**
     * 获取【请填写功能名称】详细信息
     */

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(rawmaterialInfoService.selectRawmaterialInfoById(id));
    }

    /**
     * 新增【请填写功能名称】
     */

   // @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RawmaterialInfo rawmaterialInfo)
    {
        Long nextIdStr = IdUtil.getSnowflake().nextId();
        rawmaterialInfo.setStatus("1");
        rawmaterialInfo.setRawmaterialNum(nextIdStr);
        System.err.println(rawmaterialInfo);
        return toAjax(rawmaterialInfoService.insertRawmaterialInfo(rawmaterialInfo));
    }

    /**
     * 修改【请填写功能名称】
     */
//    @RequiresPermissions("system:info:edit")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RawmaterialInfo rawmaterialInfo)
    {
        return toAjax(rawmaterialInfoService.updateRawmaterialInfo(rawmaterialInfo));
    }

    /**
     * 删除【请填写功能名称】
     */
//    @RequiresPermissions("system:info:remove")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@GetMapping ("/del")
    public AjaxResult remove(Long[] ids)
    {
        return toAjax(rawmaterialInfoService.deleteRawmaterialInfoByIds(ids));
    }

    @GetMapping("getNumList")
    public List<RawmaterialInfo> getNumList(){
        List<RawmaterialInfo> list= rawmaterialInfoService.getNumList();
        return list;
    }

}
