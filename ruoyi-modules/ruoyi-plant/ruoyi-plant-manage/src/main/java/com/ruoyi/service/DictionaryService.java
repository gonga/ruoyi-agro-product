package com.ruoyi.service;

import com.ruoyi.domain.Dictionary;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 86152
* @description 针对表【dictionary(农产品溯源字典表)】的数据库操作Service
* @createDate 2024-02-28 14:52:58
*/
public interface DictionaryService extends IService<Dictionary> {

}
