package com.ruoyi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.domain.Dictionary;

/**
* @author 86152
* @description 针对表【dictionary(农产品溯源字典表)】的数据库操作Mapper
* @createDate 2024-02-28 14:52:58
* @Entity com.ruoyi.system.api.domain.Dictionary
*/
public interface DictionaryMapper extends BaseMapper<Dictionary> {

}




