package com.ruoyi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.domain.Dictionary;
import com.ruoyi.service.DictionaryService;
import com.ruoyi.mapper.DictionaryMapper;
import org.springframework.stereotype.Service;

/**
* @author 86152
* @description 针对表【dictionary(农产品溯源字典表)】的数据库操作Service实现
* @createDate 2024-02-28 14:52:58
*/
@Service
public class DictionaryServiceImpl extends ServiceImpl<DictionaryMapper, Dictionary>
    implements DictionaryService{

}




