package com.ruoyi.mapper;

import com.ruoyi.domain.SysExamine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 傲
* @description 针对表【sys_examine(审批表)】的数据库操作Mapper
* @createDate 2024-03-04 18:41:31
* @Entity com.ruoyi.domain.SysExamine
*/
public interface SysExamineMapper extends BaseMapper<SysExamine> {

}




