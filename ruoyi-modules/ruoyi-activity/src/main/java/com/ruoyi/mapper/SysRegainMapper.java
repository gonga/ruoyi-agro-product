package com.ruoyi.mapper;

import com.ruoyi.domain.SysRegain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 傲
* @description 针对表【sys_regain(找回申请表)】的数据库操作Mapper
* @createDate 2024-03-04 18:41:31
* @Entity com.ruoyi.domain.SysRegain
*/
public interface SysRegainMapper extends BaseMapper<SysRegain> {

}




