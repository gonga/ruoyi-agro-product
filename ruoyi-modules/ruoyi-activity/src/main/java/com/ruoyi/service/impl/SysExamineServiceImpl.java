package com.ruoyi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.domain.SysExamine;
import com.ruoyi.service.SysExamineService;
import com.ruoyi.mapper.SysExamineMapper;
import org.springframework.stereotype.Service;

/**
* @author 傲
* @description 针对表【sys_examine(审批表)】的数据库操作Service实现
* @createDate 2024-03-04 18:41:31
*/
@Service
public class SysExamineServiceImpl extends ServiceImpl<SysExamineMapper, SysExamine>
    implements SysExamineService{

}




