package com.ruoyi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.domain.SysRegain;
import com.ruoyi.service.SysRegainService;
import com.ruoyi.mapper.SysRegainMapper;
import org.springframework.stereotype.Service;

/**
* @author 傲
* @description 针对表【sys_regain(找回申请表)】的数据库操作Service实现
* @createDate 2024-03-04 18:41:31
*/
@Service
public class SysRegainServiceImpl extends ServiceImpl<SysRegainMapper, SysRegain>
    implements SysRegainService{

}




