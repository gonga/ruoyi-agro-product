package com.ruoyi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 审批表
 * @TableName sys_examine
 */
@TableName(value ="sys_examine")
@Data
public class SysExamine implements Serializable {
    /**
     * 审批ID
     */
    @TableId(type = IdType.AUTO)
    private Long examineId;

    /**
     * 找回申请ID
     */
    private Long regainId;

    /**
     * 流程名称
     */
    private String examineName;

    /**
     * 审批人
     */
    private String assignee;

    /**
     * 审批条件（1代表通过 2代表驳回  3代表拒绝申请） 
     */
    private String decision;

    /**
     * 审批找回的用户ID
     */
    private Long userId;

    /**
     * 审批创建时间
     */
    private Date createTime;

    /**
     * 申请原因描述
     */
    private String describes;

    /**
     * 流程任务ID
     */
    private String taskId;

    /**
     * 流程实例ID
     */
    private String instanceId;

    /**
     * 删除标识（0正常  1删除）
     */
    private String delFlag;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}