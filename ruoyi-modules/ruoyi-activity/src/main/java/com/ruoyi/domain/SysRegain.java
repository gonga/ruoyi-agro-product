package com.ruoyi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 找回申请表
 * @TableName sys_regain
 */
@TableName(value ="sys_regain")
@Data
public class SysRegain implements Serializable {
    /**
     * 找回申请ID
     */
    @TableId(type = IdType.AUTO)
    private Long regainId;

    /**
     * 找回的删除用户ID
     */
    private Long userId;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date cteateTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 执行结果（0代表审批中 1代表已通过  2代表拒绝申请）
     */
    private String status;

    /**
     * 申请原因
     */
    private String describes;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}