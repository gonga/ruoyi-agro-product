package com.ruoyi.mapper;

import java.util.List;
import com.ruoyi.domain.CkStoragelog;

/**
 * 入库记录Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-05
 */
public interface CkStoragelogMapper
{
    /**
     * 查询入库记录
     *
     * @param storageNum 入库记录主键
     * @return 入库记录
     */
    public CkStoragelog selectCkStoragelogByStorageNum(String storageNum);

    /**
     * 查询入库记录列表
     *
     * @param ckStoragelog 入库记录
     * @return 入库记录集合
     */
    public List<CkStoragelog> selectCkStoragelogList(CkStoragelog ckStoragelog);

    /**
     * 新增入库记录
     *
     * @param ckStoragelog 入库记录
     * @return 结果
     */
    public int insertCkStoragelog(CkStoragelog ckStoragelog);

    /**
     * 修改入库记录
     *
     * @param ckStoragelog 入库记录
     * @return 结果
     */
    public int updateCkStoragelog(CkStoragelog ckStoragelog);

    /**
     * 删除入库记录
     *
     * @param storageNum 入库记录主键
     * @return 结果
     */
    public int deleteCkStoragelogByStorageNum(String storageNum);

    /**
     * 批量删除入库记录
     *
     * @param storageNums 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCkStoragelogByStorageNums(String[] storageNums);
}
