package com.ruoyi.mapper;

import java.util.List;
import com.ruoyi.domain.CkStash;

/**
 * 仓库\库区Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-05
 */
public interface CkStashMapper
{
    /**
     * 查询仓库\库区
     *
     * @param stashNum 仓库\库区主键
     * @return 仓库\库区
     */
    public CkStash selectCkStashByStashNum(String stashNum);

    /**
     * 查询仓库\库区列表
     *
     * @param ckStash 仓库\库区
     * @return 仓库\库区集合
     */
    public List<CkStash> selectCkStashList(CkStash ckStash);

    /**
     * 新增仓库\库区
     *
     * @param ckStash 仓库\库区
     * @return 结果
     */
    public int insertCkStash(CkStash ckStash);

    /**
     * 修改仓库\库区
     *
     * @param ckStash 仓库\库区
     * @return 结果
     */
    public int updateCkStash(CkStash ckStash);

    /**
     * 删除仓库\库区
     *
     * @param stashNum 仓库\库区主键
     * @return 结果
     */
    public int deleteCkStashByStashNum(String stashNum);

    /**
     * 批量删除仓库\库区
     *
     * @param stashNums 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCkStashByStashNums(String[] stashNums);
}
