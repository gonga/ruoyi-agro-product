package com.ruoyi.service;

import java.util.List;

import com.ruoyi.domain.CkDeliverylog;

/**
 * 出库记录Service接口
 *
 * @author ruoyi
 * @date 2024-03-05
 */
public interface ICkDeliverylogService
{
    /**
     * 查询出库记录
     *
     * @param deliveryNum 出库记录主键
     * @return 出库记录
     */
    public CkDeliverylog selectCkDeliverylogByDeliveryNum(String deliveryNum);

    /**
     * 查询出库记录列表
     *
     * @param ckDeliverylog 出库记录
     * @return 出库记录集合
     */
    public List<CkDeliverylog> selectCkDeliverylogList(CkDeliverylog ckDeliverylog);

    /**
     * 新增出库记录
     *
     * @param ckDeliverylog 出库记录
     * @return 结果
     */
    public int insertCkDeliverylog(CkDeliverylog ckDeliverylog);

    /**
     * 修改出库记录
     *
     * @param ckDeliverylog 出库记录
     * @return 结果
     */
    public int updateCkDeliverylog(CkDeliverylog ckDeliverylog);

    /**
     * 批量删除出库记录
     *
     * @param deliveryNums 需要删除的出库记录主键集合
     * @return 结果
     */
    public int deleteCkDeliverylogByDeliveryNums(String[] deliveryNums);

    /**
     * 删除出库记录信息
     *
     * @param deliveryNum 出库记录主键
     * @return 结果
     */
    public int deleteCkDeliverylogByDeliveryNum(String deliveryNum);
}
