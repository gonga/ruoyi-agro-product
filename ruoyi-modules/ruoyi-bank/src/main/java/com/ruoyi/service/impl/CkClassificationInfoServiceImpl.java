package com.ruoyi.service.impl;

import java.util.List;

import com.ruoyi.domain.CkClassificationInfo;
import com.ruoyi.mapper.CkClassificationInfoMapper;
import com.ruoyi.service.ICkClassificationInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 物料分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@Service
public class CkClassificationInfoServiceImpl implements ICkClassificationInfoService
{
    @Autowired
    private CkClassificationInfoMapper ckClassificationInfoMapper;

    /**
     * 查询物料分类
     * 
     * @param id 物料分类主键
     * @return 物料分类
     */
    @Override
    public CkClassificationInfo selectCkClassificationInfoById(Long id)
    {
        return ckClassificationInfoMapper.selectCkClassificationInfoById(id);
    }

    /**
     * 查询物料分类列表
     * 
     * @param ckClassificationInfo 物料分类
     * @return 物料分类
     */
    @Override
    public List<CkClassificationInfo> selectCkClassificationInfoList(CkClassificationInfo ckClassificationInfo)
    {
        return ckClassificationInfoMapper.selectCkClassificationInfoList(ckClassificationInfo);
    }

    /**
     * 新增物料分类
     * 
     * @param ckClassificationInfo 物料分类
     * @return 结果
     */
    @Override
    public int insertCkClassificationInfo(CkClassificationInfo ckClassificationInfo)
    {
        return ckClassificationInfoMapper.insertCkClassificationInfo(ckClassificationInfo);
    }

    /**
     * 修改物料分类
     * 
     * @param ckClassificationInfo 物料分类
     * @return 结果
     */
    @Override
    public int updateCkClassificationInfo(CkClassificationInfo ckClassificationInfo)
    {
        return ckClassificationInfoMapper.updateCkClassificationInfo(ckClassificationInfo);
    }

    /**
     * 批量删除物料分类
     * 
     * @param ids 需要删除的物料分类主键
     * @return 结果
     */
    @Override
    public int deleteCkClassificationInfoByIds(Long[] ids)
    {
        return ckClassificationInfoMapper.deleteCkClassificationInfoByIds(ids);
    }

    /**
     * 删除物料分类信息
     * 
     * @param id 物料分类主键
     * @return 结果
     */
    @Override
    public int deleteCkClassificationInfoById(Long id)
    {
        return ckClassificationInfoMapper.deleteCkClassificationInfoById(id);
    }
}
