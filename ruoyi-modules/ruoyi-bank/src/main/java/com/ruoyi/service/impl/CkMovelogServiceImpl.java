package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.domain.CkMovelog;
import com.ruoyi.mapper.CkMovelogMapper;
import com.ruoyi.service.ICkMovelogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 移库记录
Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@Service
public class CkMovelogServiceImpl implements ICkMovelogService
{
    @Autowired
    private CkMovelogMapper ckMovelogMapper;

    /**
     * 查询移库记录

     *
     * @param moveNum 移库记录
主键
     * @return 移库记录

     */
    @Override
    public CkMovelog selectCkMovelogByMoveNum(String moveNum)
    {
        return ckMovelogMapper.selectCkMovelogByMoveNum(moveNum);
    }

    /**
     * 查询移库记录
列表
     *
     * @param ckMovelog 移库记录

     * @return 移库记录

     */
    @Override
    public List<CkMovelog> selectCkMovelogList(CkMovelog ckMovelog)
    {
        return ckMovelogMapper.selectCkMovelogList(ckMovelog);
    }

    /**
     * 新增移库记录

     *
     * @param ckMovelog 移库记录

     * @return 结果
     */
    @Override
    public int insertCkMovelog(CkMovelog ckMovelog)
    {
        ckMovelog.setCreateTime(DateUtils.getNowDate());
        return ckMovelogMapper.insertCkMovelog(ckMovelog);
    }

    /**
     * 修改移库记录

     *
     * @param ckMovelog 移库记录

     * @return 结果
     */
    @Override
    public int updateCkMovelog(CkMovelog ckMovelog)
    {
        return ckMovelogMapper.updateCkMovelog(ckMovelog);
    }

    /**
     * 批量删除移库记录

     *
     * @param moveNums 需要删除的移库记录
主键
     * @return 结果
     */
    @Override
    public int deleteCkMovelogByMoveNums(String[] moveNums)
    {
        return ckMovelogMapper.deleteCkMovelogByMoveNums(moveNums);
    }

    /**
     * 删除移库记录
信息
     *
     * @param moveNum 移库记录
主键
     * @return 结果
     */
    @Override
    public int deleteCkMovelogByMoveNum(String moveNum)
    {
        return ckMovelogMapper.deleteCkMovelogByMoveNum(moveNum);
    }
}
