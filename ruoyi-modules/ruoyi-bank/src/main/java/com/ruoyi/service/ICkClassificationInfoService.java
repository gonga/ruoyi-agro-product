package com.ruoyi.service;

import java.util.List;

import com.ruoyi.domain.CkClassificationInfo;

/**
 * 物料分类Service接口
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public interface ICkClassificationInfoService 
{
    /**
     * 查询物料分类
     * 
     * @param id 物料分类主键
     * @return 物料分类
     */
    public CkClassificationInfo selectCkClassificationInfoById(Long id);

    /**
     * 查询物料分类列表
     * 
     * @param ckClassificationInfo 物料分类
     * @return 物料分类集合
     */
    public List<CkClassificationInfo> selectCkClassificationInfoList(CkClassificationInfo ckClassificationInfo);

    /**
     * 新增物料分类
     * 
     * @param ckClassificationInfo 物料分类
     * @return 结果
     */
    public int insertCkClassificationInfo(CkClassificationInfo ckClassificationInfo);

    /**
     * 修改物料分类
     * 
     * @param ckClassificationInfo 物料分类
     * @return 结果
     */
    public int updateCkClassificationInfo(CkClassificationInfo ckClassificationInfo);

    /**
     * 批量删除物料分类
     * 
     * @param ids 需要删除的物料分类主键集合
     * @return 结果
     */
    public int deleteCkClassificationInfoByIds(Long[] ids);

    /**
     * 删除物料分类信息
     * 
     * @param id 物料分类主键
     * @return 结果
     */
    public int deleteCkClassificationInfoById(Long id);
}
