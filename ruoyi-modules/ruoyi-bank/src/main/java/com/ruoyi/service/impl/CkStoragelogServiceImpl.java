package com.ruoyi.service.impl;

import java.util.List;

import com.ruoyi.domain.CkStoragelog;
import com.ruoyi.mapper.CkStoragelogMapper;
import com.ruoyi.service.ICkStoragelogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 入库记录Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@Service
public class CkStoragelogServiceImpl implements ICkStoragelogService
{
    @Autowired
    private CkStoragelogMapper ckStoragelogMapper;

    /**
     * 查询入库记录
     *
     * @param storageNum 入库记录主键
     * @return 入库记录
     */
    @Override
    public CkStoragelog selectCkStoragelogByStorageNum(String storageNum)
    {
        return ckStoragelogMapper.selectCkStoragelogByStorageNum(storageNum);
    }

    /**
     * 查询入库记录列表
     *
     * @param ckStoragelog 入库记录
     * @return 入库记录
     */
    @Override
    public List<CkStoragelog> selectCkStoragelogList(CkStoragelog ckStoragelog)
    {
        return ckStoragelogMapper.selectCkStoragelogList(ckStoragelog);
    }

    /**
     * 新增入库记录
     *
     * @param ckStoragelog 入库记录
     * @return 结果
     */
    @Override
    public int insertCkStoragelog(CkStoragelog ckStoragelog)
    {
        return ckStoragelogMapper.insertCkStoragelog(ckStoragelog);
    }

    /**
     * 修改入库记录
     *
     * @param ckStoragelog 入库记录
     * @return 结果
     */
    @Override
    public int updateCkStoragelog(CkStoragelog ckStoragelog)
    {
        return ckStoragelogMapper.updateCkStoragelog(ckStoragelog);
    }

    /**
     * 批量删除入库记录
     *
     * @param storageNums 需要删除的入库记录主键
     * @return 结果
     */
    @Override
    public int deleteCkStoragelogByStorageNums(String[] storageNums)
    {
        return ckStoragelogMapper.deleteCkStoragelogByStorageNums(storageNums);
    }

    /**
     * 删除入库记录信息
     *
     * @param storageNum 入库记录主键
     * @return 结果
     */
    @Override
    public int deleteCkStoragelogByStorageNum(String storageNum)
    {
        return ckStoragelogMapper.deleteCkStoragelogByStorageNum(storageNum);
    }
}
