package com.ruoyi.service;

import java.util.List;

import com.ruoyi.domain.CkMaterialInfo;

/**
 * 物料管理Service接口
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public interface ICkMaterialInfoService 
{
    /**
     * 查询物料管理
     * 
     * @param id 物料管理主键
     * @return 物料管理
     */
    public CkMaterialInfo selectCkMaterialInfoById(Long id);

    /**
     * 查询物料管理列表
     * 
     * @param ckMaterialInfo 物料管理
     * @return 物料管理集合
     */
    public List<CkMaterialInfo> selectCkMaterialInfoList(CkMaterialInfo ckMaterialInfo);

    /**
     * 新增物料管理
     * 
     * @param ckMaterialInfo 物料管理
     * @return 结果
     */
    public int insertCkMaterialInfo(CkMaterialInfo ckMaterialInfo);

    /**
     * 修改物料管理
     * 
     * @param ckMaterialInfo 物料管理
     * @return 结果
     */
    public int updateCkMaterialInfo(CkMaterialInfo ckMaterialInfo);

    /**
     * 批量删除物料管理
     * 
     * @param ids 需要删除的物料管理主键集合
     * @return 结果
     */
    public int deleteCkMaterialInfoByIds(Long[] ids);

    /**
     * 删除物料管理信息
     * 
     * @param id 物料管理主键
     * @return 结果
     */
    public int deleteCkMaterialInfoById(Long id);
}
