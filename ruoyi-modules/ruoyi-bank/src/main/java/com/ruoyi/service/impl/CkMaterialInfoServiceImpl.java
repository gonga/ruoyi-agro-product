package com.ruoyi.service.impl;

import java.util.List;

import com.ruoyi.domain.CkMaterialInfo;
import com.ruoyi.mapper.CkMaterialInfoMapper;
import com.ruoyi.service.ICkMaterialInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 物料管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@Service
public class CkMaterialInfoServiceImpl implements ICkMaterialInfoService
{
    @Autowired
    private CkMaterialInfoMapper ckMaterialInfoMapper;

    /**
     * 查询物料管理
     * 
     * @param id 物料管理主键
     * @return 物料管理
     */
    @Override
    public CkMaterialInfo selectCkMaterialInfoById(Long id)
    {
        return ckMaterialInfoMapper.selectCkMaterialInfoById(id);
    }

    /**
     * 查询物料管理列表
     * 
     * @param ckMaterialInfo 物料管理
     * @return 物料管理
     */
    @Override
    public List<CkMaterialInfo> selectCkMaterialInfoList(CkMaterialInfo ckMaterialInfo)
    {
        return ckMaterialInfoMapper.selectCkMaterialInfoList(ckMaterialInfo);
    }

    /**
     * 新增物料管理
     * 
     * @param ckMaterialInfo 物料管理
     * @return 结果
     */
    @Override
    public int insertCkMaterialInfo(CkMaterialInfo ckMaterialInfo)
    {
        return ckMaterialInfoMapper.insertCkMaterialInfo(ckMaterialInfo);
    }

    /**
     * 修改物料管理
     * 
     * @param ckMaterialInfo 物料管理
     * @return 结果
     */
    @Override
    public int updateCkMaterialInfo(CkMaterialInfo ckMaterialInfo)
    {
        return ckMaterialInfoMapper.updateCkMaterialInfo(ckMaterialInfo);
    }

    /**
     * 批量删除物料管理
     * 
     * @param ids 需要删除的物料管理主键
     * @return 结果
     */
    @Override
    public int deleteCkMaterialInfoByIds(Long[] ids)
    {
        return ckMaterialInfoMapper.deleteCkMaterialInfoByIds(ids);
    }

    /**
     * 删除物料管理信息
     * 
     * @param id 物料管理主键
     * @return 结果
     */
    @Override
    public int deleteCkMaterialInfoById(Long id)
    {
        return ckMaterialInfoMapper.deleteCkMaterialInfoById(id);
    }
}
