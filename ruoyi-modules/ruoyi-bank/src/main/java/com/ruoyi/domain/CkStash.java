package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 仓库\库区对象 ck_stash
 *
 * @author ruoyi
 * @date 2024-03-05
 */
public class CkStash extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 仓库\库区编号 */
    @Excel(name = "仓库/库区编号")
    private String stashNum;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String stashName;

    /** 仓库上级编号 */
    @Excel(name = "仓库上级编号")
    private String stashParentid;

    public void setStashNum(String stashNum)
    {
        this.stashNum = stashNum;
    }

    public String getStashNum()
    {
        return stashNum;
    }
    public void setStashName(String stashName)
    {
        this.stashName = stashName;
    }

    public String getStashName()
    {
        return stashName;
    }
    public void setStashParentid(String stashParentid)
    {
        this.stashParentid = stashParentid;
    }

    public String getStashParentid()
    {
        return stashParentid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("stashNum", getStashNum())
            .append("stashName", getStashName())
            .append("stashParentid", getStashParentid())
            .append("remark", getRemark())
            .toString();
    }
}
