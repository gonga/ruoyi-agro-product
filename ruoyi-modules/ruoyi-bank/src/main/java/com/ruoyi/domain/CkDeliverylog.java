package com.ruoyi.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 出库记录对象 ck_deliverylog
 *
 * @author ruoyi
 * @date 2024-03-05
 */
public class CkDeliverylog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 出库编号 */
    @Excel(name = "出库编号")
    private String deliveryNum;

    /** 出库类型 */
    @Excel(name = "出库类型")
    private Long deliveryType;

    /** 顾客名称 */
    @Excel(name = "顾客名称")
    private String clinetName;

    /** 物流编号 */
    @Excel(name = "物流编号")
    private String logisticsNum;

    /** 状态（0 未发货 1部分发货  2已发货 3作废） */
    @Excel(name = "状态", readConverterExp = "0=,未=发货,1=部分发货,2=已发货,3=作废")
    private String status;

    /** 物料编号 */
    @Excel(name = "物料编号")
    private String goodsNum;

    /** 源仓库编号 */
    @Excel(name = "源仓库编号")
    private String sourceNum;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal money;

    /** 逻辑删除 0：正常 1：已删除 */
    private String delFlag;

    public void setDeliveryNum(String deliveryNum)
    {
        this.deliveryNum = deliveryNum;
    }

    public String getDeliveryNum()
    {
        return deliveryNum;
    }
    public void setDeliveryType(Long deliveryType)
    {
        this.deliveryType = deliveryType;
    }

    public Long getDeliveryType()
    {
        return deliveryType;
    }
    public void setClinetName(String clinetName)
    {
        this.clinetName = clinetName;
    }

    public String getClinetName()
    {
        return clinetName;
    }
    public void setLogisticsNum(String logisticsNum)
    {
        this.logisticsNum = logisticsNum;
    }

    public String getLogisticsNum()
    {
        return logisticsNum;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setGoodsNum(String goodsNum)
    {
        this.goodsNum = goodsNum;
    }

    public String getGoodsNum()
    {
        return goodsNum;
    }
    public void setSourceNum(String sourceNum)
    {
        this.sourceNum = sourceNum;
    }

    public String getSourceNum()
    {
        return sourceNum;
    }
    public void setMoney(BigDecimal money)
    {
        this.money = money;
    }

    public BigDecimal getMoney()
    {
        return money;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("deliveryNum", getDeliveryNum())
            .append("deliveryType", getDeliveryType())
            .append("clinetName", getClinetName())
            .append("logisticsNum", getLogisticsNum())
            .append("status", getStatus())
            .append("goodsNum", getGoodsNum())
            .append("sourceNum", getSourceNum())
            .append("money", getMoney())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
