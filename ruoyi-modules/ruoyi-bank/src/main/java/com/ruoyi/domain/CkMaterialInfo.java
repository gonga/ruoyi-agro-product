package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 物料管理对象 ck_material_info
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
public class CkMaterialInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 物料编号 */
    @Excel(name = "物料编号")
    private Long num;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** 分类外键 */
    @Excel(name = "分类外键")
    private Long classificationId;

    /** 单位类别 */
    @Excel(name = "单位类别")
    private String unit;

    /** 所属仓库id */
    @Excel(name = "所属仓库id")
    private String warehouseId;

    /** 所属库区id */
    @Excel(name = "所属库区id")
    private String reservoirId;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expirationDate;

    /** 库存 */
    @Excel(name = "库存")
    private String inventory;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setNum(Long num) 
    {
        this.num = num;
    }

    public Long getNum() 
    {
        return num;
    }
    public void setMaterialName(String materialName) 
    {
        this.materialName = materialName;
    }

    public String getMaterialName() 
    {
        return materialName;
    }
    public void setClassificationId(Long classificationId) 
    {
        this.classificationId = classificationId;
    }

    public Long getClassificationId() 
    {
        return classificationId;
    }
    public void setUnit(String unit) 
    {
        this.unit = unit;
    }

    public String getUnit() 
    {
        return unit;
    }
    public void setWarehouseId(String warehouseId) 
    {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseId() 
    {
        return warehouseId;
    }
    public void setReservoirId(String reservoirId) 
    {
        this.reservoirId = reservoirId;
    }

    public String getReservoirId() 
    {
        return reservoirId;
    }
    public void setExpirationDate(Date expirationDate) 
    {
        this.expirationDate = expirationDate;
    }

    public Date getExpirationDate() 
    {
        return expirationDate;
    }
    public void setInventory(String inventory) 
    {
        this.inventory = inventory;
    }

    public String getInventory() 
    {
        return inventory;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("num", getNum())
            .append("materialName", getMaterialName())
            .append("classificationId", getClassificationId())
            .append("unit", getUnit())
            .append("warehouseId", getWarehouseId())
            .append("reservoirId", getReservoirId())
            .append("expirationDate", getExpirationDate())
            .append("inventory", getInventory())
            .toString();
    }
}
