package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 移库记录
对象 ck_movelog
 *
 * @author ruoyi
 * @date 2024-03-05
 */
public class CkMovelog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 移库记录编号 */
    @Excel(name = "移库记录编号")
    private String moveNum;

    /** 物料编号 */
    @Excel(name = "物料编号")
    private String goodsNum;

    /** 数量 */
    @Excel(name = "数量")
    private String amout;

    /** 源库编号 */
    @Excel(name = "源库编号")
    private String sourceNum;

    /** 目标库编号 */
    @Excel(name = "目标库编号")
    private String targetNum;

    /** 状态（ 0代表未操作 1代表部分移动 2代表移动完毕 3代表作废） */
    @Excel(name = "状态", readConverterExp = "0=代表未操作,1=代表部分移动,2=代表移动完毕,3=代表作废")
    private String status;

    /** 删除标识（0正常  1已删除） */
    private String delFlag;

    public void setMoveNum(String moveNum)
    {
        this.moveNum = moveNum;
    }

    public String getMoveNum()
    {
        return moveNum;
    }
    public void setGoodsNum(String goodsNum)
    {
        this.goodsNum = goodsNum;
    }

    public String getGoodsNum()
    {
        return goodsNum;
    }
    public void setAmout(String amout)
    {
        this.amout = amout;
    }

    public String getAmout()
    {
        return amout;
    }
    public void setSourceNum(String sourceNum)
    {
        this.sourceNum = sourceNum;
    }

    public String getSourceNum()
    {
        return sourceNum;
    }
    public void setTargetNum(String targetNum)
    {
        this.targetNum = targetNum;
    }

    public String getTargetNum()
    {
        return targetNum;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("moveNum", getMoveNum())
            .append("createTime", getCreateTime())
            .append("goodsNum", getGoodsNum())
            .append("amout", getAmout())
            .append("sourceNum", getSourceNum())
            .append("targetNum", getTargetNum())
            .append("status", getStatus())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
