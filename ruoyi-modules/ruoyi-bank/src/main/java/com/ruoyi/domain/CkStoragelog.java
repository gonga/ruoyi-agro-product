package com.ruoyi.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 入库记录对象 ck_storagelog
 *
 * @author ruoyi
 * @date 2024-03-05
 */
public class CkStoragelog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 入库编号 */
    @Excel(name = "入库编号")
    private String storageNum;

    /** 入库类型 */
    @Excel(name = "入库类型")
    private Long storageType;

    /** 供应商名称 */
    @Excel(name = "供应商名称")
    private String supplier;

    /** 物流编号 */
    @Excel(name = "物流编号")
    private String logisticsNum;

    /** 物料编号 */
    @Excel(name = "物料编号")
    private String goodsNum;

    /** 目标仓库 */
    @Excel(name = "目标仓库")
    private String targetNum;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal money;

    /** 0:未发货 1：在途 2：部分入库 3：入库完成 */
    @Excel(name = "0:未发货 1：在途 2：部分入库 3：入库完成")
    private String status;

    /** 删除标识（0代表正常  1代表删除） */
    private String delFlag;

    public void setStorageNum(String storageNum)
    {
        this.storageNum = storageNum;
    }

    public String getStorageNum()
    {
        return storageNum;
    }
    public void setStorageType(Long storageType)
    {
        this.storageType = storageType;
    }

    public Long getStorageType()
    {
        return storageType;
    }
    public void setSupplier(String supplier)
    {
        this.supplier = supplier;
    }

    public String getSupplier()
    {
        return supplier;
    }
    public void setLogisticsNum(String logisticsNum)
    {
        this.logisticsNum = logisticsNum;
    }

    public String getLogisticsNum()
    {
        return logisticsNum;
    }
    public void setGoodsNum(String goodsNum)
    {
        this.goodsNum = goodsNum;
    }

    public String getGoodsNum()
    {
        return goodsNum;
    }
    public void setTargetNum(String targetNum)
    {
        this.targetNum = targetNum;
    }

    public String getTargetNum()
    {
        return targetNum;
    }
    public void setMoney(BigDecimal money)
    {
        this.money = money;
    }

    public BigDecimal getMoney()
    {
        return money;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("storageNum", getStorageNum())
            .append("storageType", getStorageType())
            .append("supplier", getSupplier())
            .append("logisticsNum", getLogisticsNum())
            .append("goodsNum", getGoodsNum())
            .append("targetNum", getTargetNum())
            .append("money", getMoney())
            .append("status", getStatus())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
