package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CkClassificationInfo;
import com.ruoyi.service.ICkClassificationInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 物料分类Controller
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@RestController
@RequestMapping("/class")
public class CkClassificationInfoController extends BaseController
{
    @Autowired
    private ICkClassificationInfoService ckClassificationInfoService;

    /**
     * 查询物料分类列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CkClassificationInfo ckClassificationInfo)
    {
        startPage();
        List<CkClassificationInfo> list = ckClassificationInfoService.selectCkClassificationInfoList(ckClassificationInfo);
        return getDataTable(list);
    }

    /**
     * 导出物料分类列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, CkClassificationInfo ckClassificationInfo)
    {
        List<CkClassificationInfo> list = ckClassificationInfoService.selectCkClassificationInfoList(ckClassificationInfo);
        ExcelUtil<CkClassificationInfo> util = new ExcelUtil<CkClassificationInfo>(CkClassificationInfo.class);
        util.exportExcel(response, list, "物料分类数据");
    }

    /**
     * 获取物料分类详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(ckClassificationInfoService.selectCkClassificationInfoById(id));
    }

    /**
     * 新增物料分类
     */
    @PostMapping
    public AjaxResult add(@RequestBody CkClassificationInfo ckClassificationInfo)
    {
        return toAjax(ckClassificationInfoService.insertCkClassificationInfo(ckClassificationInfo));
    }

    /**
     * 修改物料分类
     */
    @PutMapping
    public AjaxResult edit(@RequestBody CkClassificationInfo ckClassificationInfo)
    {
        return toAjax(ckClassificationInfoService.updateCkClassificationInfo(ckClassificationInfo));
    }

    /**
     * 删除物料分类
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ckClassificationInfoService.deleteCkClassificationInfoByIds(ids));
    }
}
