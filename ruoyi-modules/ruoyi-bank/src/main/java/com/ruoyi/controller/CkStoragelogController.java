package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CkStoragelog;
import com.ruoyi.service.ICkStoragelogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 入库记录Controller
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@RestController
@RequestMapping("/storagelog")
public class CkStoragelogController extends BaseController
{
    @Autowired
    private ICkStoragelogService ckStoragelogService;

    /**
     * 查询入库记录列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CkStoragelog ckStoragelog)
    {
        startPage();
        List<CkStoragelog> list = ckStoragelogService.selectCkStoragelogList(ckStoragelog);
        return getDataTable(list);
    }

    /**
     * 导出入库记录列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, CkStoragelog ckStoragelog)
    {
        List<CkStoragelog> list = ckStoragelogService.selectCkStoragelogList(ckStoragelog);
        ExcelUtil<CkStoragelog> util = new ExcelUtil<CkStoragelog>(CkStoragelog.class);
        util.exportExcel(response, list, "入库记录数据");
    }

    /**
     * 获取入库记录详细信息
     */
    @GetMapping(value = "/{storageNum}")
    public AjaxResult getInfo(@PathVariable("storageNum") String storageNum)
    {
        return success(ckStoragelogService.selectCkStoragelogByStorageNum(storageNum));
    }

    /**
     * 新增入库记录
     */
    @PostMapping
    public AjaxResult add(@RequestBody CkStoragelog ckStoragelog)
    {
        return toAjax(ckStoragelogService.insertCkStoragelog(ckStoragelog));
    }

    /**
     * 修改入库记录
     */
    @PutMapping
    public AjaxResult edit(@RequestBody CkStoragelog ckStoragelog)
    {
        return toAjax(ckStoragelogService.updateCkStoragelog(ckStoragelog));
    }

    /**
     * 删除入库记录
     */
	@DeleteMapping("/{storageNums}")
    public AjaxResult remove(@PathVariable String[] storageNums)
    {
        return toAjax(ckStoragelogService.deleteCkStoragelogByStorageNums(storageNums));
    }
}
