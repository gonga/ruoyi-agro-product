package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CkMovelog;
import com.ruoyi.service.ICkMovelogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 移库记录
Controller
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@RestController
@RequestMapping("/movelog")
public class CkMovelogController extends BaseController
{
    @Autowired
    private ICkMovelogService ckMovelogService;

    /**
     * 查询移库记录
列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CkMovelog ckMovelog)
    {
        startPage();
        List<CkMovelog> list = ckMovelogService.selectCkMovelogList(ckMovelog);
        return getDataTable(list);
    }

    /**
     * 导出移库记录
列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, CkMovelog ckMovelog)
    {
        List<CkMovelog> list = ckMovelogService.selectCkMovelogList(ckMovelog);
        ExcelUtil<CkMovelog> util = new ExcelUtil<CkMovelog>(CkMovelog.class);
        util.exportExcel(response, list, "移库记录数据");

    }

    /**
     * 获取移库记录
详细信息
     */
    @GetMapping(value = "/{moveNum}")
    public AjaxResult getInfo(@PathVariable("moveNum") String moveNum)
    {
        return success(ckMovelogService.selectCkMovelogByMoveNum(moveNum));
    }

    /**
     * 新增移库记录

     */
    @PostMapping
    public AjaxResult add(@RequestBody CkMovelog ckMovelog)
    {
        return toAjax(ckMovelogService.insertCkMovelog(ckMovelog));
    }

    /**
     * 修改移库记录

     */
    @PutMapping
    public AjaxResult edit(@RequestBody CkMovelog ckMovelog)
    {
        return toAjax(ckMovelogService.updateCkMovelog(ckMovelog));
    }

    /**
     * 删除移库记录

     */
	@DeleteMapping("/{moveNums}")
    public AjaxResult remove(@PathVariable String[] moveNums)
    {
        return toAjax(ckMovelogService.deleteCkMovelogByMoveNums(moveNums));
    }
}
