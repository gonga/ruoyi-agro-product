package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CkMaterialInfo;
import com.ruoyi.service.ICkMaterialInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 物料管理Controller
 * 
 * @author ruoyi
 * @date 2024-03-05
 */
@RestController
@RequestMapping("/material")
public class CkMaterialInfoController extends BaseController
{
    @Autowired
    private ICkMaterialInfoService ckMaterialInfoService;

    /**
     * 查询物料管理列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CkMaterialInfo ckMaterialInfo)
    {
        startPage();
        List<CkMaterialInfo> list = ckMaterialInfoService.selectCkMaterialInfoList(ckMaterialInfo);
        return getDataTable(list);
    }

    /**
     * 导出物料管理列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, CkMaterialInfo ckMaterialInfo)
    {
        List<CkMaterialInfo> list = ckMaterialInfoService.selectCkMaterialInfoList(ckMaterialInfo);
        ExcelUtil<CkMaterialInfo> util = new ExcelUtil<CkMaterialInfo>(CkMaterialInfo.class);
        util.exportExcel(response, list, "物料管理数据");
    }

    /**
     * 获取物料管理详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(ckMaterialInfoService.selectCkMaterialInfoById(id));
    }

    /**
     * 新增物料管理
     */
    @PostMapping
    public AjaxResult add(@RequestBody CkMaterialInfo ckMaterialInfo)
    {
        return toAjax(ckMaterialInfoService.insertCkMaterialInfo(ckMaterialInfo));
    }

    /**
     * 修改物料管理
     */
    @PutMapping
    public AjaxResult edit(@RequestBody CkMaterialInfo ckMaterialInfo)
    {
        return toAjax(ckMaterialInfoService.updateCkMaterialInfo(ckMaterialInfo));
    }

    /**
     * 删除物料管理
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ckMaterialInfoService.deleteCkMaterialInfoByIds(ids));
    }
}
