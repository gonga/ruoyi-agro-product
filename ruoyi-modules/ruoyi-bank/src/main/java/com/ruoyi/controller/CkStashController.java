package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CkStash;
import com.ruoyi.service.ICkStashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 仓库\库区Controller
 *
 * @author ruoyi
 * @date 2024-03-05
 */
@RestController
@RequestMapping("/stash")
public class CkStashController extends BaseController
{
    @Autowired
    private ICkStashService ckStashService;

    /**
     * 查询仓库\库区列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CkStash ckStash)
    {
        startPage();
        List<CkStash> list = ckStashService.selectCkStashList(ckStash);
        return getDataTable(list);
    }

    /**
     * 导出仓库\库区列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, CkStash ckStash)
    {
        List<CkStash> list = ckStashService.selectCkStashList(ckStash);
        ExcelUtil<CkStash> util = new ExcelUtil<CkStash>(CkStash.class);
        util.exportExcel(response, list, "仓库/库区数据");
    }

    /**
     * 获取仓库\库区详细信息
     */
    @GetMapping(value = "/{stashNum}")
    public AjaxResult getInfo(@PathVariable("stashNum") String stashNum)
    {
        return success(ckStashService.selectCkStashByStashNum(stashNum));
    }

    /**
     * 新增仓库\库区
     */
    @PostMapping
    public AjaxResult add(@RequestBody CkStash ckStash)
    {
        return toAjax(ckStashService.insertCkStash(ckStash));
    }

    /**
     * 修改仓库\库区
     */
    @PutMapping
    public AjaxResult edit(@RequestBody CkStash ckStash)
    {
        return toAjax(ckStashService.updateCkStash(ckStash));
    }

    /**
     * 删除仓库\库区
     */
	@DeleteMapping("/{stashNums}")
    public AjaxResult remove(@PathVariable String[] stashNums)
    {
        return toAjax(ckStashService.deleteCkStashByStashNums(stashNums));
    }
}
