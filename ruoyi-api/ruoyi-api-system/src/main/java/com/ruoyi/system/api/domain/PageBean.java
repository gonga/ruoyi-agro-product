package com.ruoyi.system.api.domain;

/**
 * @Description: TODO
 * @Author 卟哭！
 * @Date 2024/03/04  18:32
 * @Version 1.0
 */
public class PageBean {

    /**
     * 当前页
     */
    private Integer pageNum;
    /**
     * 每页显示记录数
     */
    private Integer pageSize;

}
