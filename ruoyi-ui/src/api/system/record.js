import request from '@/utils/request'

// 查询加工记录跟踪列表
export function listRecord(query) {
  return request({
    url: '/factory/record/list',
    method: 'get',
    params: query
  })
}

// 查询加工记录跟踪详细
export function getRecord(processId) {
  return request({
    url: '/factory/record/' + processId,
    method: 'get'
  })
}

// 新增加工记录跟踪
export function addRecord(data) {
  return request({
    url: '/factory/record',
    method: 'post',
    data: data
  })
}

// 修改加工记录跟踪
export function updateRecord(data) {
  return request({
    url: '/factory/record',
    method: 'put',
    data: data
  })
}

// 删除加工记录跟踪
export function delRecord(processId) {
  return request({
    url: '/factory/record/' + processId,
    method: 'delete'
  })
}
// 查询农产品信息
export function rawmaterialList() {
  return request({
    url: '/factory/rawmaterial/getNumList',
    method: 'get'
  })
}
// 获取加工进度和id信息
export function getDicList(type) {
  return request({
    url: '/factory/dictionary/getStandardList?type=' + type,
    method: 'get',
  })
}
// 导出加工成品信息列表
export function exportlistInfo(query) {
  return request({
    url: '/factory/proinfo/export',
    method: 'post',
    params: query
  })
}
