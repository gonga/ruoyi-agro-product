import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listInfo(query) {
  return request({
    url: '/factory/manage/list',
    method: 'get',
    params: query
  })
}

// 查询字典表
export function Dictlist() {
  return request({
    url: '/factory/manage/diclist',
    method: 'get',
  })
}

// 查询三级联动
export function getDistict(parentId) {
  return request({
    url: '/factory/manage/dislist?parentId=' + parentId,
    method: 'get',
  })
}

// 查询【请填写功能名称】详细
export function getInfo(factoryId) {
  return request({
    url: '/factory/manage/' + factoryId,
    method: 'get'
  })
}

// 导入
export function getimportData() {
  return request({
    url: '/factory/manage/importData',
    method: 'post'
  })
}

// 导出
export function getDownload() {
  return request({
    url: '/factory/manage/export',
    method: 'post'
  })
}

// 新增【请填写功能名称】
export function addInfo(data) {
  return request({
    url: '/factory/manage/',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateInfo(data) {
  return request({
    url: '/factory/manage/',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delInfo(factoryId) {
  return request({
    url: '/factory/manage/' + factoryId,
    method: 'delete'
  })
}
