import request from '@/utils/request'

// 查询物料管理列表
export function listInfo(query) {
  return request({
    url: '/bank/material/list',
    method: 'get',
    params: query
  })
}

// 查询物料管理详细
export function getInfo(id) {
  return request({
    url: '/bank/material/' + id,
    method: 'get'
  })
}

// 新增物料管理
export function addInfo(data) {
  return request({
    url: '/bank/material',
    method: 'post',
    data: data
  })
}

// 修改物料管理
export function updateInfo(data) {
  return request({
    url: '/bank/material',
    method: 'put',
    data: data
  })
}

// 删除物料管理
export function delInfo(id) {
  return request({
    url: '/bank/material/' + id,
    method: 'delete'
  })
}
