import request from '@/utils/request'

// 查询出库记录列表
export function listDeliverylog(query) {
  return request({
    url: '/bank/deliverylog/list',
    method: 'get',
    params: query
  })
}

// 查询出库记录详细
export function getDeliverylog(deliveryNum) {
  return request({
    url: '/bank/deliverylog/' + deliveryNum,
    method: 'get'
  })
}

// 新增出库记录
export function addDeliverylog(data) {
  return request({
    url: '/bank/deliverylog',
    method: 'post',
    data: data
  })
}

// 修改出库记录
export function updateDeliverylog(data) {
  return request({
    url: '/bank/deliverylog',
    method: 'put',
    data: data
  })
}

// 删除出库记录
export function delDeliverylog(deliveryNum) {
  return request({
    url: '/bank/deliverylog/' + deliveryNum,
    method: 'delete'
  })
}
