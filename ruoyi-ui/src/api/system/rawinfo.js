import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listInfo(query) {
  return request({
    url: '/factory/rawmaterial/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getInfo(id) {
  return request({
    url: '/factory/rawmaterial/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addInfo(data) {
  return request({
    url: '/factory/rawmaterial',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateInfo(data) {
  return request({
    url: '/factory/rawmaterial',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delInfo(id) {
  return request({
    url: '/factory/rawmaterial/del?ids=' + id,
    method: 'get'
  })
}

// 获取加工进度和id信息
export function getDicList(type) {
    return request({
      url: '/factory/dictionary/getStandardList?type=' + type,
      method: 'get',
    })
  }
// 添加加工记录信息
export function AddProcess(process) {
    return request({
      url: '/factory/record/add',
      method: 'post',
      data: process
    })
  }

// 根据id获取加工记录信息
export function getProcessList(id) {
    return request({
      url: '/factory/record/queryById?id='+id,
      method: 'get',
    })
}
// 获取操作工类型
export function getopertor() {
    return request({
      url: '/factory/dictionary/getopertor',
      method: 'get',
    })
}
