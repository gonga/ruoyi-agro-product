import request from '@/utils/request'

// 查询移库记录
列表
export function listMovelog(query) {
  return request({
    url: '/bank/movelog/list',
    method: 'get',
    params: query
  })
}

// 查询移库记录
详细
export function getMovelog(moveNum) {
  return request({
    url: '/bank/movelog/' + moveNum,
    method: 'get'
  })
}

// 新增移库记录

export function addMovelog(data) {
  return request({
    url: '/bank/movelog',
    method: 'post',
    data: data
  })
}

// 修改移库记录

export function updateMovelog(data) {
  return request({
    url: '/bank/movelog',
    method: 'put',
    data: data
  })
}

// 删除移库记录

export function delMovelog(moveNum) {
  return request({
    url: '/bank/movelog/' + moveNum,
    method: 'delete'
  })
}
