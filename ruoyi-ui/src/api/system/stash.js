import request from '@/utils/request'

// 查询仓库\库区列表
export function listStash(query) {
  return request({
    url: '/bank/stash/list',
    method: 'get',
    params: query
  })
}

// 查询仓库\库区详细
export function getStash(stashNum) {
  return request({
    url: '/bank/stash/' + stashNum,
    method: 'get'
  })
}

// 新增仓库\库区
export function addStash(data) {
  return request({
    url: '/bank/stash',
    method: 'post',
    data: data
  })
}

// 修改仓库\库区
export function updateStash(data) {
  return request({
    url: '/bank/stash',
    method: 'put',
    data: data
  })
}

// 删除仓库\库区
export function delStash(stashNum) {
  return request({
    url: '/bank/stash/' + stashNum,
    method: 'delete'
  })
}
