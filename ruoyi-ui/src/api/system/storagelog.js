import request from '@/utils/request'

// 查询入库记录列表
export function listStoragelog(query) {
  return request({
    url: '/bank/storagelog/list',
    method: 'get',
    params: query
  })
}

// 查询入库记录详细
export function getStoragelog(storageNum) {
  return request({
    url: '/bank/storagelog/' + storageNum,
    method: 'get'
  })
}

// 新增入库记录
export function addStoragelog(data) {
  return request({
    url: '/bank/storagelog',
    method: 'post',
    data: data
  })
}

// 修改入库记录
export function updateStoragelog(data) {
  return request({
    url: '/bank/storagelog',
    method: 'put',
    data: data
  })
}

// 删除入库记录
export function delStoragelog(storageNum) {
  return request({
    url: '/bank/storagelog/' + storageNum,
    method: 'delete'
  })
}
