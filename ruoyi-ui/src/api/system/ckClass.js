import request from '@/utils/request'

// 查询物料分类列表
export function listInfo(query) {
  return request({
    url: '/bank/class/list',
    method: 'get',
    params: query
  })
}

// 查询物料分类详细
export function getInfo(id) {
  return request({
    url: '/bank/class/' + id,
    method: 'get'
  })
}

// 新增物料分类
export function addInfo(data) {
  return request({
    url: '/bank/class',
    method: 'post',
    data: data
  })
}

// 修改物料分类
export function updateInfo(data) {
  return request({
    url: '/bank/class',
    method: 'put',
    data: data
  })
}

// 删除物料分类
export function delInfo(id) {
  return request({
    url: '/bank/class/' + id,
    method: 'delete'
  })
}
