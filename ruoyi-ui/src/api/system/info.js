import request from '@/utils/request'

// 查询加工成品信息列表
export function listInfo(query) {
  return request({
    url: '/factory/proinfo/list',
    method: 'get',
    params: query
  })
}

// 查询加工成品信息详细
export function getInfo(productionId) {
  return request({
    url: '/factory/proinfo/' + productionId,
    method: 'get'
  })
}

// 新增加工成品信息
export function addInfo(data) {
  return request({
    url: '/factory/proinfo',
    method: 'post',
    data: data
  })
}

// 修改加工成品信息
export function updateInfo(data) {
  return request({
    url: '/factory/proinfo',
    method: 'put',
    data: data
  })
}

// 删除加工成品信息
export function delInfo(productionId) {
  return request({
    url: '/factory/proinfo/' + productionId,
    method: 'delete'
  })
}

// 获取原料编号和id信息
export function getNumList() {
  return request({
    url: '/factory/rawmaterial/getNumList',
    method: 'get'
  })
}
// 获取执行标准名称和id信息
export function getStandardList(type) {
  return request({
    url: '/factory/dictionary/getStandardList?type=' + type,
    method: 'get',
  })
}


